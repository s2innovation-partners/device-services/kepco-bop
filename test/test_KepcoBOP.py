# pylint: disable=redefined-outer-name,unused-argument
import json
import subprocess
import time

import mock
import PyTango as tango
import pytest
import scpilib


TANGO_DEVICE = "test/bop/1"
POLL_PERIOD = 1.1
WATCHDOG_PERIOD = 3


@pytest.fixture
def scpi_server_and_mock():
    # pylint: disable=bad-whitespace
    m = mock.NonCallableMagicMock()  # pylint: disable=invalid-name
    server = scpilib.scpi(port=50250)
    server.addSpecialCommand('IDN',     m.read_idn)
    server.addCommand("meas:curr",      m.read_meas_curr)
    server.addCommand("meas:volt",      m.read_meas_volt)
    server.addCommand("outp",           m.read_outp,        m.write_outp)
    server.addCommand("func:mode",      m.read_func_mode,   m.write_func_mode)
    server.addCommand("curr:ampl",      m.read_curr,        m.write_curr, default=True)  # noqa E501
    server.addCommand("curr:rang",      m.read_curr_rang,   m.write_curr_rang)
    server.addCommand("volt:ampl",      m.read_volt,        m.write_volt, default=True)  # noqa E501
    server.addCommand("volt:rang",      m.read_volt_rang,   m.write_volt_rang)
    server.addCommand("syst:err",       m.read_syst_err)
    server.addCommand("syst:rem",       m.read_syst_rem)
    server.addCommand("stat:ques:cond", m.read_stat_ques_cond)
    server.open()
    m.read_idn.return_value = "kepco,BOP,SN12345,20190123"
    m.read_meas_curr.return_value = "0"
    m.read_meas_volt.return_value = "0"
    m.read_outp.return_value = "0"
    m.read_func_mode.return_value = "0"
    m.read_curr.return_value = "0"
    m.read_curr_rang.return_value = "0"
    m.read_volt.return_value = "0"
    m.read_volt_rang.return_value = "0"
    m.read_syst_err.return_value = "NO ERROR"
    m.read_syst_rem.return_value = "0"
    m.read_stat_ques_cond.return_value = "0"
    time.sleep(0.5)
    yield (server, m)
    server.close()


@pytest.fixture
def scpi(scpi_server_and_mock):
    return scpi_server_and_mock[1]


@pytest.fixture
def proxy():
    proc = subprocess.Popen(args=["KepcoBOP", "BOP"])
    device_proxy = tango.DeviceProxy(TANGO_DEVICE)
    time.sleep(1.5)
    yield device_proxy
    proc.kill()


@pytest.fixture
def dummy_mode():
    name = "DummyMode"
    values = get_device_property(name)
    value = next(iter(values), "")
    if value.lower() != "true":
        put_device_property(name, ["true"])
    yield
    put_device_property(name, values)


def get_device_property(name):
    database = tango.Database()
    return database.get_device_property(TANGO_DEVICE, name)[name]


def put_device_property(name, values):
    datum = tango.DbDatum(name)
    for value in values:
        datum.append(value)
    database = tango.Database()
    database.put_device_property(TANGO_DEVICE, datum)


def test_read_attributes(scpi, proxy):
    meas_curr = 7324.2
    meas_volt = 6752.8
    outp = 1
    func_mode = 1
    curr = 23.12
    syst_err = "128, error"
    stat_ques_cond = 112
    scpi.read_meas_curr.return_value = str(meas_curr)
    scpi.read_meas_volt.return_value = str(meas_volt)
    scpi.read_outp.return_value = str(outp)
    scpi.read_func_mode.return_value = str(func_mode)
    scpi.read_curr.return_value = str(curr)
    scpi.read_syst_err.return_value = syst_err
    scpi.read_stat_ques_cond.return_value = str(stat_ques_cond)
    time.sleep(POLL_PERIOD)
    assert proxy.MeasureActualCurrent == meas_curr
    assert proxy.MeasureActualVoltage == meas_volt
    assert proxy.PowerSupplyOutput == outp
    assert proxy.OperatingModePowerSupply == func_mode
    assert proxy.CurrentSetpoint == curr
    assert proxy.ErrorMessages == syst_err
    assert proxy.QuestionableConditionRegister == stat_ques_cond


def test_write_current_setpoint(scpi, proxy):
    value = 34211.6
    proxy.CurrentSetpoint = value
    time.sleep(0.1)
    scpi.write_curr.assert_called_once_with(str(value))


def test_startup_into_standby(scpi, proxy):
    assert proxy.State() == tango.DevState.STANDBY
    assert proxy.Status() == "The device is in STANDBY state."


def test_reinit_into_on_if_outp_is_enabled(scpi, proxy):
    scpi.read_outp.return_value = "1"
    proxy.Init()
    time.sleep(POLL_PERIOD)
    assert proxy.State() == tango.DevState.ON
    assert proxy.Status() == "The device is in ON state."


def test_reinit_into_standby_if_outp_is_disabled(scpi, proxy):
    scpi.read_outp.return_value = "0"
    proxy.Init()
    time.sleep(POLL_PERIOD)
    assert proxy.State() == tango.DevState.STANDBY
    assert proxy.Status() == "The device is in STANDBY state."


def test_startup_into_fault_if_scpi_is_not_accessible(proxy):
    assert proxy.State() == tango.DevState.FAULT
    assert proxy.Status() == (
        "The device is in FAULT state.\n"
        "Cannot connect to the instrument.\n")
    assert proxy.PowerSupplyOutput == -1


def test_startup_with_dummy_mode_if_scpi_not_accessible(dummy_mode, proxy):
    assert proxy.State() == tango.DevState.STANDBY
    assert proxy.Status() == "The device is in STANDBY state."


def test_scpi_communication_failure(scpi_server_and_mock, proxy):
    scpi, mock = scpi_server_and_mock
    mock.read_meas_curr.return_value = 123

    time.sleep(POLL_PERIOD)
    assert proxy.State() == tango.DevState.STANDBY
    assert proxy.Status() == "The device is in STANDBY state."
    assert proxy.PowerSupplyOutput == 0
    assert proxy.SetRemoteMode == 0
    assert proxy.MeasureActualCurrent != 0

    scpi.close()
    time.sleep(WATCHDOG_PERIOD)
    time.sleep(POLL_PERIOD)
    assert proxy.State() == tango.DevState.FAULT
    assert proxy.Status() == (
        "The device is in FAULT state.\n"
        "Cannot connect to the instrument.\n")
    assert proxy.PowerSupplyOutput == -1
    assert proxy.SetRemoteMode == -1
    assert proxy.MeasureActualCurrent == 0

    scpi.open()
    time.sleep(WATCHDOG_PERIOD)
    time.sleep(POLL_PERIOD)
    assert proxy.State() == tango.DevState.STANDBY
    assert proxy.Status() == "The device is in STANDBY state."
    assert proxy.PowerSupplyOutput == 0
    assert proxy.SetRemoteMode == 0
    assert proxy.MeasureActualCurrent != 0


def test_attribute_config_for_power_supply_output(scpi, proxy):
    json_config = proxy.getAttrConfig("PowerSupplyOutput")
    config = json.loads(json_config)
    assert config["ctrl"] == "combo"
    assert config["name"] == "PowerSupplyOutput"
    assert config["source"] == "e:UNKNOWN;OFF;ON"


def test_attribute_config_for_non_existing_attribute(scpi, proxy):
    assert proxy.getAttrConfig("ForSureNotExists") == ""


def test_read_stat_ques_cond(scpi):

    proc = subprocess.Popen(
        args=["KepcoBOP", "BOP"],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    time.sleep(1.5)

    scpi.read_stat_ques_cond.return_value = 0b11000
    time.sleep(2 * POLL_PERIOD)

    scpi.read_stat_ques_cond.return_value = 0
    time.sleep(2 * POLL_PERIOD)

    proc.kill()
    logs = [line for line in proc.stderr.readlines()
            if "STAT:QUES:COND" in line and "DEBUG" not in line]

    assert len(logs) == 2

    assert "ERROR" in logs[0]
    assert (
        "STAT:QUES:COND indicates error, "
        "value: 24, "
        "bits: [0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], "
        "errors at: [3, 4]") in logs[0]

    assert "INFO" in logs[1]
    assert "STAT:QUES:COND is ok, value: 0" in logs[1]
