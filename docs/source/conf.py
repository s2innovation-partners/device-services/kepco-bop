import sys
import os
conf_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(conf_dir, os.path.pardir))
sys.path.insert(0, os.path.join(conf_dir, os.path.pardir, os.path.pardir))

extensions = ['sphinx.ext.autodoc', 'devicedoc']
master_doc = 'index'

project = u'Kepco BOP Device Server'
copyright = u""""""
