# -*- coding: utf-8 -*-
#
# This file is part of the KepcoBOP project
#
#
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

"""

"""

from . import release
from .KepcoBOP import KepcoBOP, main

__version__ = release.version
__version_info__ = release.version_info
__author__ = release.author
