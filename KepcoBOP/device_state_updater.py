import PyTango as tango


class DeviceStateUpdater(object):

    def __init__(self, device):
        self.device = device
        self.skippy_state = tango.DevState.UNKNOWN
        self.device_output = 0
        self.skippy_status = ""
        self.status_buffer = ""

    def skippy_state_changed(self, state):
        self.skippy_state = state
        self._update_tango_state()

    def skippy_status_changed(self, status):
        self.skippy_status = status

    def device_output_changed(self, output):
        self.device_output = output
        self._update_tango_state()

    def _update_tango_state(self):
        new_state = self._calculate_state()
        self.device.set_state(new_state)
        self._reset_polling_buffer(new_state)

    def _calculate_state(self):
        s = tango.DevState  # pylint: disable=invalid-name
        if self.skippy_state in (s.ON, s.RUNNING):
            return s.ON if self.device_output == 1 else s.STANDBY
        elif self.skippy_state in (s.FAULT, s.DISABLE):
            return s.FAULT
        else:
            return s.UNKNOWN

    def _reset_polling_buffer(self, new_state):
        if new_state in (tango.DevState.FAULT, tango.DevState.UNKNOWN):
            if self.device.polling_buffer:
                self.device.polling_buffer.reset()

    def get_status(self, base_status):
        extra = "\n".join(self.skippy_status.split("\n")[1:])
        status = "{}\n{}".format(base_status, extra) if extra else base_status
        self.status_buffer = status  # copy to prevent daingling pointer
        return self.status_buffer
