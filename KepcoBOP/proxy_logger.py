import logging
logger = logging.getLogger(__name__)

try:
    from loggerstreams import ProxyLogger as PrevacProxyLogger  # pylint: disable=unused-import

    class ProxyLogger(object):
        # pylint: disable=invalid-name

        @staticmethod
        def sendError(self, msg):
            try:
                PrevacProxyLogger.sendError(self, msg)
            except Exception:
                logger.error(msg)

        @staticmethod
        def sendFatal(self, msg):
            try:
                PrevacProxyLogger.sendFatal(self, msg)
            except Exception:
                logger.critical(msg)

        @staticmethod
        def sendWarning(self, msg):
            try:
                PrevacProxyLogger.sendWarning(self, msg)
            except Exception:
                logger.warning(msg)

        @staticmethod
        def sendDebug(self, msg):
            try:
                PrevacProxyLogger.sendDebug(self, msg)
            except Exception as e:
                print(e)
                logger.debug(msg)

        @staticmethod
        def sendInfo(self, msg):
            try:
                PrevacProxyLogger.sendInfo(self, msg)
            except Exception:
                logger.info(msg)

except Exception:  # pylint: disable=broad-except

    class ProxyLogger(object):
        # pylint: disable=invalid-name

        @staticmethod
        def sendError(_self, msg):
            logger.error(msg)

        @staticmethod
        def sendFatal(_self, msg):
            logger.critical(msg)

        @staticmethod
        def sendWarning(_self, msg):
            logger.warning(msg)

        @staticmethod
        def sendDebug(_self, msg):
            logger.debug(msg)

        @staticmethod
        def sendInfo(_self, msg):
            logger.info(msg)
