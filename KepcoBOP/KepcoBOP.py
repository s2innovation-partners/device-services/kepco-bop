# -*- coding: utf-8 -*-
#
# This file is part of the KepcoBOP project
#
#
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" 

"""

# PyTango imports
import PyTango
from PyTango import DebugIt
from PyTango.server import run
from PyTango.server import Device, DeviceMeta
from PyTango.server import attribute, command
from PyTango.server import device_property
from PyTango import AttrQuality, DispLevel, DevState
from PyTango import AttrWriteType
# Additional import
# PROTECTED REGION ID(KepcoBOP.additionnal_import) ENABLED START #
import json
import logging

from skippylib.skippy import Skippy as SkippyObj

from .device_state_updater import DeviceStateUpdater
from .stat_ques_cond_evaluator import StatQuesCondEvaluator
from .device_callbacks import DeviceCallbacks
from .polling_buffer import PollingBuffer
from .dummy_scpi import DummyScpi


CMD_MEAS_CURR = "MEAS:CURR?"
CMD_MEAS_VOLT = "MEAS:VOLT?"
CMD_OUTP = "OUTP?"
CMD_FUNC_MODE = "FUNC:MODE?"
CMD_CURR = "CURR?"
CMD_CURR_RANG = "CURR:RANG?"
CMD_VOLT = "VOLT?"
CMD_VOLT_RANG = "VOLT:RANG?"
CMD_SYST_ERR = "SYST:ERR?"
CMD_SYST_REM = "SYST:REM?"
CMD_STAT_QUES_COND = "STAT:QUES:COND?"

CMD_W_OUTP = "OUTP {}".format
CMD_W_FUNC_MODE = "FUNC:MODE {}".format
CMD_W_CURR = "CURR {}".format
CMD_W_CURR_RANG = "CURR:RANG {}".format
CMD_W_VOLT = "VOLT {}".format
CMD_W_VOLT_RANG = "VOLT:RANG {}".format
CMD_W_SYST_REM = "SYST:REM {}".format

COMMANDS_WITH_DEFAULTS = {
    CMD_MEAS_CURR: "0",
    CMD_MEAS_VOLT: "0",
    CMD_OUTP: "-1",
    CMD_FUNC_MODE: "0",
    CMD_CURR: "0",
    CMD_CURR_RANG: "0",
    CMD_VOLT: "0",
    CMD_VOLT_RANG: "0",
    CMD_SYST_ERR: "NO ERROR",
    CMD_SYST_REM: "-1",
    CMD_STAT_QUES_COND: "0"
}

COMMANDS_WITH_MANDATORY_POLLING = [
    CMD_OUTP,
    CMD_STAT_QUES_COND
]

POLLED_COMMANDS = COMMANDS_WITH_MANDATORY_POLLING + [
    CMD_SYST_ERR,
    CMD_SYST_REM
]
# PROTECTED REGION END #    //  KepcoBOP.additionnal_import

__all__ = ["KepcoBOP", "main"]


class KepcoBOP(Device):
    """
    """
    __metaclass__ = DeviceMeta
    # PROTECTED REGION ID(KepcoBOP.class_variable) ENABLED START #
    def __init__(self, *args, **kwargs):
        self.device_state_updater = None
        self.stat_ques_cond_evaluator = None
        self.device_callbacks = None
        self.skippy = None
        self.polling_buffer = None
        super(KepcoBOP, self).__init__(*args, **kwargs)

    def dev_status(self):
        base_status = super(KepcoBOP, self).dev_status()
        return self.device_state_updater.get_status(base_status)

    def evaluate_stat_ques_cond(self):
        value = int(self.polling_buffer.Read(CMD_STAT_QUES_COND))
        self.stat_ques_cond_evaluator.evaluate(value)

    def evaluate_outp(self):
        outp = self.read_outp()
        if outp in (0, 1):
            self.device_state_updater.device_output_changed(outp)

    def read_outp(self):
        return self.read_tribool_attribute(CMD_OUTP)

    def read_tribool_attribute(self, command):
        try:
            response = self.polling_buffer.Read(command)
            value = int(response)
            if value not in (-1, 0, 1):
                raise ValueError("{} cannot be {}".format(command, value))
            return value
        except Exception:
            return -1
    # PROTECTED REGION END #    //  KepcoBOP.class_variable

    # -----------------
    # Device Properties
    # -----------------

    DeviceAddress = device_property(
        dtype='str', default_value="localhost"
    )

    DevicePort = device_property(
        dtype='uint', default_value=50250
    )

    DummyMode = device_property(
        dtype='bool', default_value=False
    )

    # ----------
    # Attributes
    # ----------

    MeasureActualCurrent = attribute(
        dtype='double',
        unit="Amper",
        display_unit="A",
        doc="Measures actual current. This query returns the actual value of output current (measured at the out-put  terminals)  as  determined  by  the  programmed  value  of  voltage  and  current  and  load  conditions.",
    )

    MeasureActualVoltage = attribute(
        dtype='double',
        unit="Volt",
        display_unit="V",
        doc="Measures actual voltage. This query returns the actual value of output voltage (measured at the out-put  terminals)  as  determined  by  the  programmed  value  of  voltage  and  current  and  load  conditions.",
    )

    PowerSupplyOutput = attribute(
        dtype='int',
        access=AttrWriteType.READ_WRITE,
        doc="Enables or disables the power supply output. Upon power up the power supply is in Local mode:the output is always on and the front panel controls affect the unit`s operation.",
    )

    OperatingModePowerSupply = attribute(
        dtype='int',
        access=AttrWriteType.READ_WRITE,
        doc="Establishes  the  operating  mode  of  the  power  supply.  0  =  Voltage  mode.  1  =  Currentmode.",
    )

    CurrentSetpoint = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        unit="Amper",
        display_unit="A",
        doc="Returns  either  the  programmed  value,  maximum  value,  or  minimum  value  of  current.  The CURR? query returns the programmed value of current. Actual output current will depend on load conditions.",
    )

    CurrentLimit = attribute(
        dtype='int',
        access=AttrWriteType.READ_WRITE,
        unit="Percent",
        display_unit="%",
        doc="Allows the user to specify the operating range for control of output current. Range = 100% allows control of output current from 0 to full scale. Range = 40% allows the full range of the 16 bit D to A con-verter to control 1/4 of the full scale output current.",
    )

    VoltageSetpoint = attribute(
        dtype='double',
        access=AttrWriteType.READ_WRITE,
        unit="Volt",
        display_unit="V",
        doc="Sets programmed voltage level at power supply output. This command programs output voltageto a specific value; actual output voltage will depend on load conditions.",
    )

    VoltageLimit = attribute(
        dtype='int',
        access=AttrWriteType.READ_WRITE,
        unit="Percent",
        display_unit="%",
        doc="Allows the user to specify the operating range for the active mode (either voltage or current).When in voltage mode this command establishes the voltage range, in current mode it establishes thecurrent  range.  Range  =  1  allows  control  of  output  voltage  or  current  from  0  to  full  scale.  Range  =  4allows the full range of the 16 bit D to A converter to control 1/4 of the full scale output voltage or cur-rent to provide greater accuracy.",
    )

    ErrorMessages = attribute(
        dtype='str',
        doc="Posts  error  messages  to  the  output  queue.  Returns  the  next  error  number  followed  by  its  corre-sponding  error  message  string  from  the  instrument  error  queue.  The  error  queue  is  a  FIFO  (first  in,first out) buffer that stores errors as they occur. As it is read, each error is removed from the queueand  the  next  error  message  is  made  available.  When  all  errors  have  been  read,  the  query  returns 0,”No error”",
    )

    SetRemoteMode = attribute(
        dtype='int',
        access=AttrWriteType.READ_WRITE,
        doc="Used  during  serial  (RS  232)  communication  to  set  the  unit  to  remote  (1  or  ON)  or  local  (0  orOFF)  mode.  This  command  must  be  issued  prior  to  commands  that  affect  the  power  supply  output(e.g., VOLT 10;:OUTP ON) to ensure the unit is in remote mode.",
    )

    QuestionableConditionRegister = attribute(
        dtype='int',
        doc="Returns  the  value  of  the  Questionable  Condition  Register  (see  Table  B-5).  The  QuestionableCondition  Register  contains  unlatched  real-time  information  about  questionable  conditions  of  thepower supply.Bit set to 1 = condition enabled (active, true); bit reset to 0 = condition disabled (inac-tive,  false).",
    )

    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        Device.init_device(self)
        # PROTECTED REGION ID(KepcoBOP.init_device) ENABLED START #
        self.device_state_updater = DeviceStateUpdater(self)
        self.stat_ques_cond_evaluator = StatQuesCondEvaluator(self)
        self.device_callbacks = DeviceCallbacks(
            self,
            self.device_state_updater)
        if self.DummyMode:
            self.skippy = DummyScpi(
                self.device_callbacks,
                COMMANDS_WITH_DEFAULTS)
        else:
            self.skippy = SkippyObj(
                parent=self.device_callbacks,
                name=self.DeviceAddress,
                port=self.DevicePort,
                nChannels=0,
                nFunctions=0,
                nMultiple=[])
            self.skippy.Instrument = ""
            setattr(self.skippy, "_Skippy__unbuilder", lambda: None)
        self.polling_buffer = PollingBuffer(
            self.skippy,
            COMMANDS_WITH_DEFAULTS,
            POLLED_COMMANDS)
        # PROTECTED REGION END #    //  KepcoBOP.init_device

    def always_executed_hook(self):
        # PROTECTED REGION ID(KepcoBOP.always_executed_hook) ENABLED START #
        pass
        # PROTECTED REGION END #    //  KepcoBOP.always_executed_hook

    def delete_device(self):
        # PROTECTED REGION ID(KepcoBOP.delete_device) ENABLED START #
        if self.skippy._watchdog:
            self.skippy._watchdog.finish()
        self.skippy.Off()
        # PROTECTED REGION END #    //  KepcoBOP.delete_device

    # ------------------
    # Attributes methods
    # ------------------

    def read_MeasureActualCurrent(self):
        # PROTECTED REGION ID(KepcoBOP.MeasureActualCurrent_read) ENABLED START #
        return float(self.polling_buffer.Read(CMD_MEAS_CURR))
        # PROTECTED REGION END #    //  KepcoBOP.MeasureActualCurrent_read

    def read_MeasureActualVoltage(self):
        # PROTECTED REGION ID(KepcoBOP.MeasureActualVoltage_read) ENABLED START #
        return float(self.polling_buffer.Read(CMD_MEAS_VOLT))
        # PROTECTED REGION END #    //  KepcoBOP.MeasureActualVoltage_read

    def read_PowerSupplyOutput(self):
        # PROTECTED REGION ID(KepcoBOP.PowerSupplyOutput_read) ENABLED START #
        return self.read_outp()
        # PROTECTED REGION END #    //  KepcoBOP.PowerSupplyOutput_read

    def write_PowerSupplyOutput(self, value):
        # PROTECTED REGION ID(KepcoBOP.PowerSupplyOutput_write) ENABLED START #
        if value in (0, 1):
            self.skippy.Write(CMD_W_OUTP(value))
        # PROTECTED REGION END #    //  KepcoBOP.PowerSupplyOutput_write

    def read_OperatingModePowerSupply(self):
        # PROTECTED REGION ID(KepcoBOP.OperatingModePowerSupply_read) ENABLED START #
        return int(self.polling_buffer.Read(CMD_FUNC_MODE))
        # PROTECTED REGION END #    //  KepcoBOP.OperatingModePowerSupply_read

    def write_OperatingModePowerSupply(self, value):
        # PROTECTED REGION ID(KepcoBOP.OperatingModePowerSupply_write) ENABLED START #
        if value in (0, 1):
            self.skippy.Write(CMD_W_FUNC_MODE(value))
        # PROTECTED REGION END #    //  KepcoBOP.OperatingModePowerSupply_write

    def read_CurrentSetpoint(self):
        # PROTECTED REGION ID(KepcoBOP.CurrentSetpoint_read) ENABLED START #
        return float(self.polling_buffer.Read(CMD_CURR))
        # PROTECTED REGION END #    //  KepcoBOP.CurrentSetpoint_read

    def write_CurrentSetpoint(self, value):
        # PROTECTED REGION ID(KepcoBOP.CurrentSetpoint_write) ENABLED START #
        self.skippy.Write(CMD_W_CURR(value))
        # PROTECTED REGION END #    //  KepcoBOP.CurrentSetpoint_write

    def read_CurrentLimit(self):
        # PROTECTED REGION ID(KepcoBOP.CurrentLimit_read) ENABLED START #
        """Reading Current value limit.
        THIS DOESN'T WORK FOR NOW.
        MAYBE DEVICE DOES NOT SUPPORT IT"""
        value = self.polling_buffer.Read(CMD_CURR_RANG)
        if value == '' or len(value) > 3:
            return 0
        return int(value)
        # PROTECTED REGION END #    //  KepcoBOP.CurrentLimit_read

    def write_CurrentLimit(self, value):
        # PROTECTED REGION ID(KepcoBOP.CurrentLimit_write) ENABLED START #
        """Writing Current value limit.
        THIS DOESN'T WORK FOR NOW.
        MAYBE DEVICE DOES NOT SUPPORT IT"""
        if value in (1, 4):
            self.skippy.Write(CMD_W_CURR_RANG(value))
        # PROTECTED REGION END #    //  KepcoBOP.CurrentLimit_write

    def read_VoltageSetpoint(self):
        # PROTECTED REGION ID(KepcoBOP.VoltageSetpoint_read) ENABLED START #
        return float(self.polling_buffer.Read(CMD_VOLT))
        # PROTECTED REGION END #    //  KepcoBOP.VoltageSetpoint_read

    def write_VoltageSetpoint(self, value):
        # PROTECTED REGION ID(KepcoBOP.VoltageSetpoint_write) ENABLED START #
        self.skippy.Write(CMD_W_VOLT(value))
        # PROTECTED REGION END #    //  KepcoBOP.VoltageSetpoint_write

    def read_VoltageLimit(self):
        # PROTECTED REGION ID(KepcoBOP.VoltageLimit_read) ENABLED START #
        """Reading Voltage value limit from device
        THIS DOESN'T WORK FOR NOW.
        MAYBE DEVICE DOES NOT SUPPORT IT"""
        value = self.polling_buffer.Read(CMD_VOLT_RANG)
        if value == '' or len(value) > 3:
            return 0
        return int(value)
        # PROTECTED REGION END #    //  KepcoBOP.VoltageLimit_read

    def write_VoltageLimit(self, value):
        # PROTECTED REGION ID(KepcoBOP.VoltageLimit_write) ENABLED START #
        """Writting Voltage value limit to device
        THIS DOESN'T WORK FOR NOW.
        MAYBE DEVICE DOES NOT SUPPORT IT"""
        if value in (1, 4):
            self.skippy.Write(CMD_W_VOLT_RANG(value))
        # PROTECTED REGION END #    //  KepcoBOP.VoltageLimit_write

    def read_ErrorMessages(self):
        # PROTECTED REGION ID(KepcoBOP.ErrorMessages_read) ENABLED START #
        return self.polling_buffer.Read(CMD_SYST_ERR)
        # PROTECTED REGION END #    //  KepcoBOP.ErrorMessages_read

    def read_SetRemoteMode(self):
        # PROTECTED REGION ID(KepcoBOP.SetRemoteMode_read) ENABLED START #
        return self.read_tribool_attribute(CMD_SYST_REM)
        # PROTECTED REGION END #    //  KepcoBOP.SetRemoteMode_read

    def write_SetRemoteMode(self, value):
        # PROTECTED REGION ID(KepcoBOP.SetRemoteMode_write) ENABLED START #
        if value in (1, 0):
            self.skippy.Write(CMD_W_SYST_REM(value))
        # PROTECTED REGION END #    //  KepcoBOP.SetRemoteMode_write

    def read_QuestionableConditionRegister(self):
        # PROTECTED REGION ID(KepcoBOP.QuestionableConditionRegister_read) ENABLED START #
        return int(self.polling_buffer.Read(CMD_STAT_QUES_COND))
        # PROTECTED REGION END #    //  KepcoBOP.QuestionableConditionRegister_read


    # --------
    # Commands
    # --------

    @command(
    dtype_in='str', 
    doc_in="Attribute name.", 
    dtype_out='str', 
    doc_out="Attribute configuration.", 
    )
    @DebugIt()
    def getAttrConfig(self, argin):
        # PROTECTED REGION ID(KepcoBOP.getAttrConfig) ENABLED START #
        if argin in ("PowerSupplyOutput", "SetRemoteMode"):
            return json.dumps({
                "ctrl": "combo",
                "def_val": "0",
                "disabled": "",
                "max_val": "1",
                "tag": "",
                "link": "",
                "label": "",
                "unit": "",
                "desc": "",
                "group": "",
                "name": argin,
                "format": "",
                "min_val": "-1",
                "source": "e:UNKNOWN;OFF;ON",
                "type": "int32"
            })
        else:
            return ""
        # PROTECTED REGION END #    //  KepcoBOP.getAttrConfig

    @command(
    )
    @DebugIt()
    def UpdateAttributes(self):
        # PROTECTED REGION ID(KepcoBOP.UpdateAttributes) ENABLED START #
        self.polling_buffer.update()
        self.evaluate_stat_ques_cond()
        self.evaluate_outp()
        # PROTECTED REGION END #    //  KepcoBOP.UpdateAttributes

    def is_UpdateAttributes_allowed(self):
        # PROTECTED REGION ID(KepcoBOP.is_UpdateAttributes_allowed) ENABLED START #
        return self.get_state() not in [DevState.FAULT]
        # PROTECTED REGION END #    //  KepcoBOP.is_UpdateAttributes_allowed

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(KepcoBOP.main) ENABLED START #

    command_info = KepcoBOP.UpdateAttributes.__tango_command__[1]
    try:
        config_dict = command_info[2]
    except IndexError:
        command_info.append({})
        config_dict = command_info[2]
    config_dict['Polling Period'] = 1000

    log_format = "%(asctime)s %(levelname)s [%(threadName)s] (%(name)s) %(filename)s:%(lineno)d: %(message)s"
    logging.basicConfig(format=log_format, level=logging.DEBUG)

    import skippylib.skippy
    from skippylib.builder import Builder
    skippylib.skippy.identifier = lambda *_args:  Builder(name="Builder")

    from skippylib.communications import BySocket
    BySocket._terminator = "\r\n"

    return run((KepcoBOP,), args=args, **kwargs)
    # PROTECTED REGION END #    //  KepcoBOP.main

if __name__ == '__main__':
    main()
