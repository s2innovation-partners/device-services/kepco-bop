from .proxy_logger import ProxyLogger


class DeviceCallbacks(object):
    def __init__(self, device, device_state_updater):
        self.device = device
        self.device_state_updater = device_state_updater

    def set_state(self, state):
        self.device_state_updater.skippy_state_changed(state)

    def set_status(self, status):
        self.device_state_updater.skippy_status_changed(status)

    def debug_stream(self, message):
        ProxyLogger.sendDebug(self.device, message)

    def info_stream(self, message):
        ProxyLogger.sendInfo(self.device, message)

    def warn_stream(self, message):
        ProxyLogger.sendWarning(self.device, message)

    def error_stream(self, message):
        ProxyLogger.sendError(self.device, message)

    def fatal_stream(self, message):
        ProxyLogger.sendFatal(self.device, message)

    def push_change_event(self, *_args):
        pass
