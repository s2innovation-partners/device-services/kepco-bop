import PyTango as tango


class DummyScpi(object):

    def __init__(self, device_callbacks, commands):
        self.commands = commands.copy()
        self.commands["OUTP?"] = "0"
        device_callbacks.set_state(tango.DevState.ON)

    def Read(self, command):  # pylint: disable=invalid-name
        return self.commands[command]

    def Write(self, _command):  # pylint: disable=invalid-name,no-self-use
        return ""

    def Off(self):  # pylint: disable=invalid-name,no-self-use
        pass

    @property
    def _watchdog(self):
        return DummyWatchdog()


class DummyWatchdog(object):

    def finish(self):
        pass
