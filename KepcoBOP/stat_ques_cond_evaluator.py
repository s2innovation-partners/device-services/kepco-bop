from .proxy_logger import ProxyLogger


# based on https://www.envox.hr/eez/bench-power-supply/psu-scpi-reference-manual/psu-scpi-registers-and-queues.html  # noqa E501 pylint: disable=line-too-long
# table in 3.4. QUEStionable Status Register
STAT_QUES_COND_ERROR_BITS = [
    3,  # TIME
    4,  # TEMP
    12,  # FAN
    13  # INST
]


def to_bitstring(number, width):
    bits = '{:0{width}b}'.format(number, width=width)
    bitstring = [int(b) for b in list(bits)]
    bitstring.reverse()
    return bitstring


class StatQuesCondEvaluator(object):

    def __init__(self, device):
        self.device = device
        self.last_value = 0

    def evaluate(self, value):
        if value != self.last_value:
            self.last_value = value
            bits = to_bitstring(value, width=16)
            errors = [i for i in STAT_QUES_COND_ERROR_BITS if bits[i]]
            self.log_message(value, bits, errors)

    def log_message(self, value, bits, errors):
        if errors:
            message = (
                "STAT:QUES:COND indicates error, "
                "value: {value}, "
                "bits: {bits}, "
                "errors at: {errors}"
            ).format(value=value, bits=bits, errors=errors)
            ProxyLogger.sendError(self.device, message)
        else:
            message = (
                "STAT:QUES:COND is ok, "
                "value: {value}"
            ).format(value=value)
            ProxyLogger.sendInfo(self.device, message)
