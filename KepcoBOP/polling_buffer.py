class PollingBuffer(object):

    def __init__(self, skippy, defaults, polled_commands):
        self.skippy = skippy
        self.defaults = defaults
        self.polled_commands = polled_commands
        self.buffer = {}
        self.reset()

    def Read(self, command):  # pylint: disable=invalid-name
        if command in self.buffer:
            return self.buffer[command]
        else:
            return self._read_or_default(command)

    def reset(self):
        for command in self.polled_commands:
            self.buffer[command] = self.defaults.get(command, "0")

    def update(self):
        for command in self.polled_commands:
            self.buffer[command] = self._read_or_default(command)

    def _read_or_default(self, command):
        default = self.defaults.get(command, "0")
        try:
            value = self.skippy.Read(command)
            return value if value else default
        except Exception:  # pylint: disable=broad-except
            return default
