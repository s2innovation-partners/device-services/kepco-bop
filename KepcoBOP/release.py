# -*- coding: utf-8 -*-
#
# This file is part of the KepcoBOP project
#
#
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

"""Release information for Python Package"""

name = """tangods-kepcobop"""
version = "1.0.0"
version_info = version.split(".")
description = """"""
author = "konrad.lata"
author_email = "konrad.lata at s2innovation.com"
license = """GPL"""
url = """www.tango-controls.org"""
copyright = """"""
