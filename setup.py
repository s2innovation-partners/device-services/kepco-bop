#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This file is part of the KepcoBOP project
#
#
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

import os
import sys
from setuptools import setup


package_name = "KepcoBOP"


try:
    from tangosetup import CustomInstall
    import pathlib2
    cmdclass = {'install': CustomInstall}
    CustomInstall.is_device_server = True
    CustomInstall.repository_path = pathlib2.Path(__file__).parent.resolve()
    CustomInstall.package_name = package_name
    version, _, _, _, _ = CustomInstall.getRepositoryInformation()
except Exception as exception:
    print("Cannot configure CustomInstall, reason: {}".format(exception))
    cmdclass = {}
    version = "0.1.0"


setup_dir = os.path.dirname(os.path.abspath(__file__))

# make sure we use latest info from local code
sys.path.insert(0, setup_dir)

readme_filename = os.path.join(setup_dir, 'README.rst')
with open(readme_filename) as file:
    long_description = file.read()

release_filename = os.path.join(setup_dir, 'KepcoBOP', 'release.py')
exec(open(release_filename).read())

pack = ['KepcoBOP']

setup(name=package_name,
      cmdclass=cmdclass,
      version=version,
      description='',
      packages=pack,
      include_package_data=True,
      test_suite="test",
      entry_points={'console_scripts': ['KepcoBOP = KepcoBOP:main']},
      author='konrad.lata',
      author_email='konrad.lata at s2innovation.com',
      license='GPL',
      long_description=long_description,
      url='www.tango-controls.org',
      platforms="All Platforms")
