Kepco BOP Tango Device Server
=============================

To be done in MOXA
------------------

- Using NPort Search Utility Search bridge and open Console IPv4.
- Set basic setup (IP,  mask, static ip)
- In Serial Port Settings, choose serial port number and Go to Operation Modes,
- On Application choose: Socket, Mode: TCP Server, TCP port: (some port), Delimiter 1: 0d and Enable it, Delimiter 2: 0a and Enable it
- On Delimiter process choose Do Nothing. Click Submmit and Save/Restart

- On Kepco BOP 100-10MG in General Setup/Interface Settings set:
    - Serial Interface Settings:
        - Boundrate: 9600
        - XON/XOFF: Disable
        - Prompt: Disable

Requirements
------------

- PyTango >= 8.1.6
    apt install python-pytango

- pytest 4.6 (for using tests)
    apt install pytest==4.6
    pip install typing

- scpilib
    git clone https://github.com/srgblnch/python-scpilib.git && cd python-scpilib && pip install .

- skippylib
    git clone https://github.com/srgblnch/skippy.git && cd skippy && pip install .

- mock (mocking tests)
    pip install mock

- NPort Search Utility or configure MOXA in linux using manual from official page.
    Windows: https://www.moxa.com/Moxa/media/PDIM/S100000199/moxa-device-search-utility-v2.3.zip

Installation
------------

Run in project repo pip install .

Usage
-----

Now you can start your device server in any
Terminal or console by calling it :
KepcoBOP instance_name

Usage with device
------------------

- Connect MOXA to computer using ethernet port
- Configure MOXA and set serial port
- Connect serial to device
- Set IP and Port in DeviceServer
- Start DeviceServer
